<?php
if ($argc !== 2) {
    printf("Usage: php %s <input.txt>\n", $argv[0]);
    exit(1);
}

$input = trim(file_get_contents($argv[1]));

test();

$result = process($input);

printf("Part 1: %d\n", $result);

function test()
{
    $tests = [
        '1122' => 3,
        '1111' => 4,
        '1234' => 0,
        '91212129' => 9,
    ];

    foreach ($tests as $input => $expectedResult) {
        $actualResult = process($input);

        if ($actualResult !== $expectedResult) {
            throw new LogicException(sprintf(
                'Self test failed. Input: %s, Expected: %s, Actual: %s',
                $input,
                $expectedResult,
                $actualResult,
            ));
        }
    }
}

function process(string $input)
{
    $symbolCount = strlen($input);

    $sum = 0;
    $previousValue = null;

    for ($i = 0; $i < $symbolCount; ++$i) {
        $currentValue = (int) $input[$i];
        $isLast = $i + 1 === $symbolCount;

        if ($previousValue === null) {
            $previousValue = $currentValue;
            continue;
        }

        if ($previousValue === $currentValue) {
            $sum += $currentValue;
        }

        if ($isLast && (int) $input[0] === $currentValue) {
            $sum += $currentValue;
        }

        $previousValue = $currentValue;
    }

    return $sum;
}
