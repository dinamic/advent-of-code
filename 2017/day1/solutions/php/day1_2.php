<?php
if ($argc !== 2) {
    printf("Usage: php %s <input.txt>\n", $argv[0]);
    exit(1);
}

$input = trim(file_get_contents($argv[1]));

test();

$result = process($input);

printf("Part 2: %d\n", $result);

function test()
{
    $tests = [
        '1212' => 6,
        '1221' => 0,
        '123425' => 4,
        '123123' => 12,
        '12131415' => 4,
    ];

    foreach ($tests as $input => $expectedResult) {
        $actualResult = process($input);

        if ($actualResult !== $expectedResult) {
            throw new LogicException(sprintf(
                'Self test failed. Input: %s, Expected: %s, Actual: %s',
                $input,
                $expectedResult,
                $actualResult,
            ));
        }
    }
}

function process(string $input)
{
    $symbolCount = strlen($input);

    $sum = 0;
    $modifier = $symbolCount / 2;

    for ($i = 0; $i < $symbolCount; ++$i) {
        $currentValue = (int) $input[$i];
        $halfWayIndex = $i + $modifier;

        while ($halfWayIndex >= $symbolCount) {
            $halfWayIndex -= $symbolCount;
        }

        $halfWayValue = (int) $input[$halfWayIndex];

        if ($currentValue === $halfWayValue) {
            $sum += $currentValue;
        }
    }

    return $sum;
}
