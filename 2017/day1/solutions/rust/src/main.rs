extern crate clap;

use clap::{App, Arg};
use std::collections::HashMap;
use std::fs::read_to_string;
use std::process::exit;

fn main() {
    let app = App::new("AoC - 2017, day 1")
        .author("Nikola Petkanski <nikola@petkanski.com>")
        .about("Solves AdventOfCode puzzle 2017, day 1")
        .arg(
            Arg::with_name("file")
                .required(true)
                .takes_value(true)
                .help("puzzle input file"),
        )
        .arg(
            Arg::with_name("part")
                .required(true)
                .takes_value(true)
                .possible_values(["1", "2"].as_ref())
                .help("part"),
        )
        .get_matches();

    let filename = app.value_of("file").unwrap().trim();
    let raw_data = read_to_string(filename).unwrap();

    let input_data: String = raw_data
        .chars()
        .enumerate()
        .filter(|(_, value)| {
            return value.is_numeric();
        })
        .map(|(_, value)| -> String {
            return value.to_string();
        })
        .collect::<Vec<String>>()
        .join("");

    let puzzle = Puzzle {
        input_data,
        part: app.value_of("part").unwrap().parse().unwrap(),
    };

    puzzle.clone().test();

    println!("Answer: {}", puzzle.solve());
}

#[derive(Clone, Debug)]
struct Puzzle {
    input_data: String,
    part: u8,
}

impl Puzzle {
    pub fn solve(self) -> u32 {
        Puzzle::process(self.part, self.input_data)
    }

    pub fn test(self) {
        match self.part {
            1 => Puzzle::test_part1(),
            2 => Puzzle::test_part2(),
            _ => panic!("Unknown part number"),
        }
    }

    fn process(part: u8, input_data: String) -> u32 {
        match part {
            1 => Puzzle::process_part1(input_data),
            2 => Puzzle::process_part2(input_data),
            _ => panic!("Unknown part number"),
        }
    }

    fn test_part1() {
        let mut tests = HashMap::new();

        tests.insert("1122", 3);
        tests.insert("1111", 4);
        tests.insert("1234", 0);
        tests.insert("91212129", 9);

        for (input_data, expected_result) in tests.iter() {
            let actual_result = Puzzle::process_part1(input_data.to_string());
            let expected_result: u32 = expected_result.to_owned();

            if actual_result == expected_result {
                continue;
            }

            println!(
                "Self test failed. Input: {}, Expected: {}, Actual: {}",
                input_data, expected_result, actual_result
            );

            exit(1);
        }
    }

    fn process_part1(input_data: String) -> u32 {
        let symbol_count = input_data.len();
        let mut sum: u32 = 0;
        let mut previous_value: u32 = 0;
        let mut first_value: u32 = 0;

        for (current_position, current_value) in input_data.chars().enumerate() {
            let current_value: u32 = current_value.to_digit(10).unwrap();
            let is_last = current_position + 1 == symbol_count;

            if current_position == 0 {
                previous_value = current_value;
                first_value = current_value;
                continue;
            }

            if previous_value == current_value {
                sum += current_value;
            }

            if is_last && current_value == first_value {
                sum += current_value;
            }

            previous_value = current_value;
        }

        sum
    }

    fn test_part2() {
        let mut tests = HashMap::new();

        tests.insert("1212", 6);
        tests.insert("1221", 0);
        tests.insert("123425", 4);
        tests.insert("123123", 12);
        tests.insert("12131415", 4);

        for (input_data, expected_result) in tests.iter() {
            let actual_result = Puzzle::process_part2(input_data.to_string());
            let expected_result: u32 = expected_result.to_owned();

            if actual_result == expected_result {
                continue;
            }

            println!(
                "Self test failed. Input: {}, Expected: {}, Actual: {}",
                input_data, expected_result, actual_result
            );

            exit(1);
        }
    }

    fn process_part2(input_data: String) -> u32 {
        let chars = input_data.chars();
        let symbol_count = input_data.len();
        let modifier = symbol_count / 2;
        let mut sum: u32 = 0;

        for (current_position, current_value) in input_data.chars().enumerate() {
            let current_value: u32 = current_value.to_digit(10).unwrap();
            let halfway_index = (current_position + modifier) % symbol_count;

            let halfway_value = chars
                .clone()
                .nth(halfway_index)
                .unwrap()
                .to_digit(10)
                .unwrap();

            if current_value == halfway_value {
                sum += current_value;
            }
        }

        sum
    }
}
