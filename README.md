# [Advent of code](http://adventofcode.com/)

### 2023

- [day 1](2023/day01/input.txt)
  - [task 1](2023/day01/day1_1.md) - [php](2023/day01/php/day1_1.php)
  - [task 2](2023/day01/day1_2.md) - [php](2023/day01/php/day1_2.php)
- [day 2](2023/day02/input.txt)
  - [task 1](2023/day02/day2_1.md) - [php](2023/day02/php/day2_1.php)
  - [task 2](2023/day02/day2_2.md) - [php](2023/day02/php/day2_2.php)
- [day 3](2023/day03/input.txt)
  - [task 1](2023/day03/day3_1.md) - [php](2023/day03/php/day3_1.php)
  - [task 2](2023/day03/day3_2.md) - 

### 2022

- [day 1](2022/day1/input.txt)
  - [task 1](2022/day1/day1_1.md) - [php](2022/day1/php/day1.php)
  - [task 2](2022/day1/day1_2.md) - [php](2022/day1/php/day1.php)
- [day 2](2022/day2/input.txt)
  - [task 1](2022/day2/day2_1.md) - [php](2022/day2/php/day2.php)
  - [task 2](2022/day2/day2_2.md) - [php](2022/day2/php/day2.php)
- [day 3](2022/day3/input.txt)
  - [task 1](2022/day3/day3_1.md) - [php](2022/day3/php/day3.php)
  - [task 2](2022/day3/day3_2.md) - [php](2022/day3/php/day3.php)
- [day 4](2022/day4/input.txt)
  - [task 1](2022/day4/day4_1.md) - [php](2022/day4/php/day4.php)
  - [task 2](2022/day4/day4_2.md) - [php](2022/day4/php/day4.php)
- [day 5](2022/day5/input.txt)
  - [task 1](2022/day5/day5_1.md) - [php](2022/day5/php/day5.php)
  - [task 2](2022/day5/day5_2.md) - [php](2022/day5/php/day5.php)
- [day 6](2022/day6/input.txt)
  - [task 1](2022/day6/day6_1.md) - [php](2022/day6/php/day6.php)
  - [task 2](2022/day6/day6_2.md) - [php](2022/day6/php/day6.php)

### 2019

- [day 1](2019/day1/inputs/input1.txt)
  - [task 1](2019/day1/day1_1.md) - [php](2019/day1/solutions/php/day1_1.php)
  - [task 2](2019/day1/day1_2.md) - [php](2019/day1/solutions/php/day1_2.php)
- [day 2](2019/day2/inputs/input1.txt)
  - [task 1](2019/day2/day2_1.md) - [php](2019/day2/solutions/php/day2.php)
  - [task 2](2019/day2/day2_2.md) - [php](2019/day2/solutions/php/day2.php)


### 2017

- [day 1](2017/day1/inputs/input1.txt)
  - [task 1](2017/day1/day1_1.md) - [php](2017/day1/solutions/php/day1_1.php) | [rust](2017/day1/solutions/rust/src/main.rs)
  - [task 2](2017/day1/day1_2.md) - [php](2017/day1/solutions/php/day1_2.php) | [rust](2017/day1/solutions/rust/src/main.rs)


### 2015

- [day 1](2015/day01/input.txt)
  - [task 1](2015/day01/day01_1.md) - [php](2015/day01/php/day1_1.php) | [rust](2015/day01/rust/src/main.rs) | [golang](2015/day01/golang/main.go)
  - [task 2](2015/day01/day01_2.md) - [php](2015/day01/php/day1_2.php) | [rust](2015/day01/rust/src/main.rs) | [golang](2015/day01/golang/main.go)
- [day 2](2015/day2_input.txt)
  - [task 1](2015/day2_1.md) - [php](2015/day2_1.php) | [rust](2015/day2_rust/src/main.rs)
  - [task 2](2015/day2_2.md) - [php](2015/day2_2.php) | [rust](2015/day2_rust/src/main.rs)
- [day 3](2015/day3_input.txt)
  - [task 1](2015/day3_1.md) - [php](2015/day3_1.php)
  - [task 2](2015/day3_2.md) - [php](2015/day3_2.php) | [x++](2015/day3_2.cpp)
- [day 4](2015/day4_input.txt)
  - [task 1](2015/day4_1.md) - [php](2015/day4_1.php)
  - [task 2](2015/day4_2.md) - [php](2015/day4_2.php)
- [day 5](2015/day5_input.txt)
  - [task 1](2015/day5_1.md) - [php](2015/day5_1.php)
  - [task 2](2015/day5_2.md) - [php](2015/day5_2.php)
- [day 6](2015/day6_input.txt)
  - [task 1](2015/day6_1.md) - [php](2015/day6_1.php)
  - [task 2](2015/day6_2.md) - [php](2015/day6_2.php)
- [day 7](2015/day7_input.txt)
  - [task 1](2015/day7_1.md) - [php](2015/day7_1.php)
  - [task 2](2015/day7_2.md) - [php](2015/day7_2.php)
- [day 8](2015/day8_input.txt)
  - [task 1](2015/day8_1.md) - [php](2015/day8_1.php)
  - [task 2](2015/day8_2.md) - [php](2015/day8_2.php)
