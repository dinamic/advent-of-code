<?php

$input = file_get_contents('day4_input.txt');

$i = 1;

while (true) {
    $hash = md5($input.$i);

    if (substr($hash, 0, 6) !== '000000') {
        ++$i;
        continue;
    }

    break;
}

printf("Found it! hash: %s, answer: %s\n", $hash, $i);
