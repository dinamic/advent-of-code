<?php

/** @var string[] $input */
$input = file('day5_input.txt');
$niceStrings = array();

foreach ($input as $line) {
    printf("Inspecting line: %s", $line);

    $containsInvalidStrings = 1 === preg_match('#(ab|cd|pq|xy)#i', $line);
    if ($containsInvalidStrings) {
        printf("  - has invalid strings\n");
        continue;
    }

    $containsVowels = 3 <= preg_match_all('#(a|e|i|o|u)#i', $line);
    if (false === $containsVowels) {
        printf("  - does not contain three vowels\n");
        continue;
    }

    $containsRepeatedCharacters = 1 === preg_match('#(\w)\1{1,}#i', $line);
    if (false === $containsRepeatedCharacters) {
        printf("  - contains repeated characters\n");
        continue;
    }

    $niceStrings[] = $line;
}

printf("Found %d nice strings.\n", count($niceStrings));

//var_dump($niceStrings);