<?php

define('DIRECTION_UP', '^');
define('DIRECTION_DOWN', 'v');
define('DIRECTION_LEFT', '<');
define('DIRECTION_RIGHT', '>');

define('COORDINATE_PREFIX_X', 'x');
define('COORDINATE_PREFIX_Y', 'y');

$input = file_get_contents('day3_input.txt');
$length = strlen($input);

$housesVisited = array();

$realSanta = new Santa;
$roboSanta = new Santa;

for ($i = 0; $i< $length; ++$i) {
    $character = $input[$i];

    /**
     * Every first house is visited by Santa and every second one is visited by RoboSanta.
     */
    if (0 === $i % 2) {
        $visitedHouseCoordinates = $realSanta->visitHouse($character);
    } else {
        $visitedHouseCoordinates = $roboSanta->visitHouse($character);
    }

    if (!array_key_exists($visitedHouseCoordinates, $housesVisited)) {
        $housesVisited[$visitedHouseCoordinates] = 0;
    }

    ++$housesVisited[$visitedHouseCoordinates];
}

printf("Found it: %s\n", count($housesVisited));

class Santa {
    /**
     * @var int
     */
    protected $x;

    /**
     * @var int
     */
    protected $y;

    public function __construct()
    {
        $this->x = 0;
        $this->y = 0;
    }

    /**
     * Returns the just visited coordinates. Keeps the current coordinates internal.
     *
     * @param string $character
     * @return string
     */
    public function visitHouse($character)
    {
        if ($character === DIRECTION_UP) {
            ++$this->x;
        } elseif ($character === DIRECTION_DOWN) {
            --$this->x;
        } elseif ($character === DIRECTION_RIGHT) {
            ++$this->y;
        } elseif ($character === DIRECTION_LEFT) {
            --$this->y;
        } else {
            die('whaaaat? -'. $character);
        }

        return $this->getArrayKey();
    }

    /**
     * Templates the answer while visiting coordinates.
     *
     * @return string
     */
    protected function getArrayKey()
    {
        return sprintf(
            '%s%s_%s%s',
            COORDINATE_PREFIX_X,
            $this->x,
            COORDINATE_PREFIX_Y,
            $this->y
        );
    }
}

