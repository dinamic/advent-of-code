use std::fs::File;
use std::io::{BufReader, BufRead};
use regex::Regex;

fn main() {
    let file = File::open("input.txt").unwrap();
    let reader = BufReader::new(file);

    let pattern = Regex::new(r"(?P<length>\d+)x(?P<width>\d+)x(?P<height>\d+)").unwrap();

    let mut total_area = 0;
    let mut ribbon_length = 0;

    for line in reader.lines() {
        let line = line.unwrap();
        let matches = pattern.captures(line.as_str()).unwrap();

        let length: i32 = matches["length"].parse().unwrap();
        let width: i32 = matches["width"].parse().unwrap();
        let height: i32 = matches["height"].parse().unwrap();

        let mut sides = [
            length * width,
            width * height,
            height * length
        ];

        let mut dimensions = [
            length,
            width,
            height
        ];

        sides.sort();
        dimensions.sort();

        let slack_area = sides[0];
        let shortest_distance: i32 = dimensions[0] + dimensions[1];
        let surface_area: i32 = sides.iter().sum();

        let ribbon_present_wrap = shortest_distance * 2;
        let ribbon_bow_length = length * width * height;

        let box_area = slack_area + 2 * surface_area;
        let box_ribbon = ribbon_present_wrap + ribbon_bow_length;

        total_area += box_area;
        ribbon_length += box_ribbon;
    }

    println!("Total square feet of wrapping paper needed: {}", total_area);
    println!("Total ribbon length needed: {}", ribbon_length);
}
