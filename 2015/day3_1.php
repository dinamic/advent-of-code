<?php

define('DIRECTION_UP', '^');
define('DIRECTION_DOWN', 'v');
define('DIRECTION_LEFT', '<');
define('DIRECTION_RIGHT', '>');

define('COORDINATE_PREFIX_X', 'x');
define('COORDINATE_PREFIX_Y', 'y');

$input = file_get_contents('day3_input.txt');
$length = strlen($input);

$x = 0;
$y = 0;

$houses = array();

for ($i = 0; $i< $length; ++$i) {
    $character = $input[$i];

    if ($character === DIRECTION_UP) {
        ++$x;
    } elseif ($character === DIRECTION_DOWN) {
        --$x;
    } elseif ($character === DIRECTION_RIGHT) {
        ++$y;
    } elseif ($character === DIRECTION_LEFT) {
        --$y;
    } else {
        die('whaaaat? -'. $character);
    }

    visitHouse($houses, $x, $y);
}

$numberOfHouses = 0;

foreach($houses as $x => $y) {
    foreach ($y as $value) {
        ++$numberOfHouses;
    }
}

echo "Found it: {$numberOfHouses}\n";

function visitHouse(array &$houses, $x, $y)
{
    $keyX = COORDINATE_PREFIX_X .$x;
    $keyY = COORDINATE_PREFIX_Y .$y;

    if (!array_key_exists($keyX, $houses)) {
        $houses[$keyX] = array();
    }

    if (!array_key_exists($keyY, $houses[$keyX])) {
        $houses[$keyX][$keyY] = 0;
    }

    ++$houses[$keyX][$keyY];

    return $houses;
}
