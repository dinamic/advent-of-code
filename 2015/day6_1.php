<?php

/** @var string[] $input */
$input = file('day6_input.txt');

$grid = new Grid(1000, 1000);
$grid->process($input);

echo 'Found it: ', $grid->getLightsLitCount(), "\n";

class Grid
{
    /**
     * @var SplFixedArray[]
     */
    protected $lights;

    public function __construct($sizeX, $sizeY)
    {
        $this->lights = new SplFixedArray($sizeX);

        for ($i = 0; $i < $this->lights->count(); ++$i) {
            $this->lights[$i] = new SplFixedArray($sizeY);
        }
    }

    public function process(array $input)
    {
        foreach ($input as $line) {
            $this->handleTurnOn($line);
            $this->handleTurnOff($line);
            $this->handleToggle($line);
        }
    }

    protected function handleTurnOff($line)
    {
        $regex = '#turn off (?<startX>\d+),(?<startY>\d+) through (?<endX>\d+),(?<endY>\d+)#i';

        $matches = array();

        if (!preg_match($regex, $line, $matches)) {
            return;
        }

        $this->changeLightStatus($matches['startX'], $matches['startY'], $matches['endX'], $matches['endY'], false);
    }

    protected function handleTurnOn($line)
    {
        $regex = '#turn on (?<startX>\d+),(?<startY>\d+) through (?<endX>\d+),(?<endY>\d+)#i';

        $matches = array();

        if (!preg_match($regex, $line, $matches)) {
            return;
        }

        $this->changeLightStatus($matches['startX'], $matches['startY'], $matches['endX'], $matches['endY'], true);
    }

    protected function handleToggle($line)
    {
        $regex = '#toggle (?<startX>\d+),(?<startY>\d+) through (?<endX>\d+),(?<endY>\d+)#i';

        $matches = array();

        if (!preg_match($regex, $line, $matches)) {
            return;
        }

        $this->changeLightStatus($matches['startX'], $matches['startY'], $matches['endX'], $matches['endY'], null);
    }

    /**
     * @param int $startX
     * @param int $startY
     * @param int $endX
     * @param int $endY
     * @param bool|null $status If null is passed, the current value is toggled
     * @return $this;
     */
    protected function changeLightStatus($startX, $startY, $endX, $endY, $status)
    {
        $startX = (int) $startX;
        $endX = (int) $endX;
        $startY = (int) $startY;
        $endY = (int) $endY;

        for ($x = $startX; $x <= $endX; ++$x) {
            for ($y = $startY; $y <= $endY; ++$y) {
                $newValue = $status;

                if (null === $status) {
                    $newValue = !$this->lights[$x][$y];
                }

                $this->lights[$x][$y] = $newValue;
            }
        }

        return $this;
    }

    public function getLightsLitCount()
    {
        $count = 0;

        foreach ($this->lights as $row) {
            $iterator = new LightsLitFilter($row);

            foreach ($iterator as $value) {
                ++$count;
            }
        }

        return $count;
    }
}

class LightsLitFilter extends FilterIterator
{
    /**
     * Check whether the current element of the iterator is acceptable
     * @link http://php.net/manual/en/filteriterator.accept.php
     * @return bool true if the current element is acceptable, otherwise false.
     * @since 5.1.0
     */
    public function accept()
    {
        $value = $this->getInnerIterator()->current();

        return true === $value;
    }
}