<?php
$input = fopen('day2_input.txt', 'r');
$regex = '/(?<length>\d+)x(?<width>\d+)x(?<height>\d+)/i';

$totalRibbonLength = 0;

while (!feof($input)) {
    $matches = array();
    $content = fgets($input);

    $hasMatched = preg_match_all($regex, $content, $matches);

    if (!$hasMatched) {
        throw new LogicException(sprintf(
            'Line does not match: %s',
            $content
        ));
    }

    $length = (int) $matches['length'][0];
    $width = (int) $matches['width'][0];
    $height = (int) $matches['height'][0];

    $perimeters = array(
        'length' => $length,
        'width'  => $width,
        'height' => $height,
    );

    asort($perimeters, SORT_NUMERIC);

    $smallestSide = array_shift($perimeters);
    $secondSmallestSide = array_shift($perimeters);

    $ribbonWrapLength = $smallestSide * 2 + $secondSmallestSide * 2;
    $ribbonBowLength = $length * $width * $height;

    $totalRibbonLength += $ribbonWrapLength + $ribbonBowLength;
}

echo "Found it: {$totalRibbonLength}\n";
