<?php

/** @var string[] $input */
$input = file('day6_input.txt');

$grid = new Grid(1000, 1000);
$grid->process($input);

echo 'Found it: ', $grid->getTotalBrightness(), "\n";

class Grid
{
    /**
     * @var SplFixedArray[]
     */
    protected $lights;

    public function __construct($sizeX, $sizeY)
    {
        $this->lights = new SplFixedArray($sizeX);

        for ($i = 0; $i < $this->lights->count(); ++$i) {
            $this->lights[$i] = new SplFixedArray($sizeY);
        }
    }

    public function process(array $input)
    {
        foreach ($input as $line) {
            $this->handleTurnOn($line);
            $this->handleTurnOff($line);
            $this->handleToggle($line);
        }
    }

    protected function handleTurnOff($line)
    {
        $regex = '#turn off (?<startX>\d+),(?<startY>\d+) through (?<endX>\d+),(?<endY>\d+)#i';

        $matches = array();

        if (!preg_match($regex, $line, $matches)) {
            return;
        }

        $this->dimLights($matches['startX'], $matches['startY'], $matches['endX'], $matches['endY'], -1);
    }

    protected function handleTurnOn($line)
    {
        $regex = '#turn on (?<startX>\d+),(?<startY>\d+) through (?<endX>\d+),(?<endY>\d+)#i';

        $matches = array();

        if (!preg_match($regex, $line, $matches)) {
            return;
        }

        $this->dimLights($matches['startX'], $matches['startY'], $matches['endX'], $matches['endY'], 1);
    }

    protected function handleToggle($line)
    {
        $regex = '#toggle (?<startX>\d+),(?<startY>\d+) through (?<endX>\d+),(?<endY>\d+)#i';

        $matches = array();

        if (!preg_match($regex, $line, $matches)) {
            return;
        }

        $this->dimLights($matches['startX'], $matches['startY'], $matches['endX'], $matches['endY'], 2);
    }

    /**
     * @param int $startX
     * @param int $startY
     * @param int $endX
     * @param int $endY
     * @param int $status
     * @return $this;
     */
    protected function dimLights($startX, $startY, $endX, $endY, $status)
    {
        $startX = (int) $startX;
        $endX = (int) $endX;
        $startY = (int) $startY;
        $endY = (int) $endY;

        for ($x = $startX; $x <= $endX; ++$x) {
            for ($y = $startY; $y <= $endY; ++$y) {
                if (null === $this->lights[$x][$y]) {
                    $this->lights[$x][$y] = 0;
                }

                $this->lights[$x][$y] += $status;

                if ($this->lights[$x][$y] < 0) {
                    $this->lights[$x][$y] = 0;
                }
            }
        }

        return $this;
    }

    public function getLightsLitCount()
    {
        $count = 0;

        foreach ($this->lights as $row) {
            $iterator = new LightsLitFilter($row);

            foreach ($iterator as $value) {
                ++$count;
            }
        }

        return $count;
    }

    public function getTotalBrightness()
    {
        $count = 0;

        foreach ($this->lights as $row) {
            foreach ($row as $value) {
                if ($value <= 0) {
                    continue;
                }

                $count += $value;
            }
        }

        return $count;
    }
}

class LightsLitFilter extends FilterIterator
{
    /**
     * Check whether the current element of the iterator is acceptable
     * @link http://php.net/manual/en/filteriterator.accept.php
     * @return bool true if the current element is acceptable, otherwise false.
     * @since 5.1.0
     */
    public function accept()
    {
        $value = $this->getInnerIterator()->current();

        return 1 <= $value;
    }
}