<?php

/** @var string[] $input */
$input = file('day5_input.txt');
$niceStrings = array();

/*$input = array(
    'qjhvhtzxzqqjkmpb',
    'xxyxx',
    'abcdefeghi',
    'xyxa',
    'efea',
    'aaax',
    'uurcxstgmygtbstg',
    'ieodomkazucvgmuy',
);
*/
foreach ($input as $line) {
    printf("Inspecting line: %s", $line);

    if (!containsPairOfLetters($line)) {
        printf("    - does not contain pair of letters\n");
        continue;
    }

    if (!containsRepeatedLetter($line)) {
        printf("    - does not contain a repeated letter\n");
        continue;
    }

    $niceStrings[] = $line;
}

printf("Found %d nice strings.\n", count($niceStrings));

function containsPairOfLetters ($line)
{
    $count = strlen($line);

    for ($i = 0;$i < $count;++$i) {
        // ignore the last position
        if ($i+2 === $count) {
            continue;
        }

        $string = substr($line, $i, 2);

        $parts = explode($string, $line);

        if (count($parts) >= 3) {
            return true;
        }
    }

    return false;
}

function containsRepeatedLetter ($line)
{
    return 0 !== preg_match_all('#(\w).{1}\1#i', $line);
}
