<?php
$input = fopen('day2_input.txt', 'r');
$regex = '/(?<length>\d+)x(?<width>\d+)x(?<height>\d+)/i';

$totalSquareFeetAmount = 0;

while (!feof($input)) {
    $matches = array();
    $content = fgets($input);

    $hasMatched = preg_match_all($regex, $content, $matches);

    if (!$hasMatched) {
        throw new LogicException(sprintf(
            'Line does not match: %s',
            $content
        ));
    }

    $length = (int) $matches['length'][0];
    $width = (int) $matches['width'][0];
    $height = (int) $matches['height'][0];

    $totalSquareFeetAmount += getLineSquareFeet($length, $width, $height);
}

echo "Found it: {$totalSquareFeetAmount}\n";

function getLineSquareFeet($length, $width, $height)
{
    $smallestSide = getSmallestSide($length, $width, $height);

    return (2 * $length * $width) + (2 * $width * $height) + (2 * $height * $length) + $smallestSide;
}

function getSmallestSide($length, $width, $height)
{
    $temporaryValue = ($length * $width);
    $smallestSide = $temporaryValue;

    $temporaryValue = ($width * $height);
    if ($temporaryValue < $smallestSide) {
        $smallestSide = $temporaryValue;
    }

    $temporaryValue = ($height * $length);
    if ($temporaryValue < $smallestSide) {
        $smallestSide = $temporaryValue;
    }

    return $smallestSide;
}
