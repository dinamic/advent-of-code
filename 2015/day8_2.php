<?php

/** @var string[] $input */
$input = file('day8_input.txt');
$total = 0;

foreach ($input as $line) {
    $line = preg_replace("(\r|\n)", '', $line);
    echo "Inspecting line: ";
    var_dump($line);

    $total += countNewlyEncodedCharacters($line);
    $total -= countStringLiterals($line);
    echo "\n";
}

echo "Found it: {$total}\n";

function countStringLiterals($string)
{
    return mb_strlen($string);
}

function countNewlyEncodedCharacters($string)
{
    $string = addslashes($string);
    $string = '"'. $string .'"';

    return mb_strlen($string);
}
