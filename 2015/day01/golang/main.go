package main

import (
	"fmt"
	"os"
	"strings"
)

func ReadFile(path string) string {
	content, err := os.ReadFile(path)

	if err != nil {
		panic(err)
	}

	return strings.TrimRight(string(content), "\n")
}

func main() {
	pwd, err := os.Getwd()

	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	input := ReadFile(pwd + "/../input.txt")

	var floor int = 0

	floor += strings.Count(input, string("("))
	floor -= strings.Count(input, string(")"))

	fmt.Println("Santa ended up on floor ", floor)

	floor = 0

	for i, char := range input {
		if char == '(' {
			floor++
		} else if char == ')' {
			floor--
		}

		if floor == -1 {
			fmt.Println("Santa entered the basement at position ", i+1)
			break
		}
	}
}
