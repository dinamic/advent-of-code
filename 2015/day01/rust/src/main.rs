use std::path::Path;
use std::fs::File;
use std::io::Read;

fn main() {
    let path = Path::new("input.txt");
    let mut file = File::open(&path).unwrap();
    let mut contents = String::new();
    file.read_to_string(&mut contents).unwrap();

    let mut floor = 0;

    for (i, c) in contents.chars().enumerate() {
        if c == '(' {
            floor += 1;
        }

        if c == ')' {
            floor -= 1;
        }

        if floor == -1 {
            println!("Santa went to the basement on {}th move.", i + 1);
        }
    }

    println!("Santa ended on floor {}", floor);
}
