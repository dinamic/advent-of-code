<?php
$input = file_get_contents(__DIR__ . '/../input.txt');

$i = $floor = 0;
$count = strlen($input);

for ($i = 0; $i < $count; ++$i) {
    if ($input[$i] === '(') {
        ++$floor;
    } else {
        --$floor;
    }

    if ($floor === -1) {
        printf('Found it: %d', ($i +1));
        break;
    }
}
