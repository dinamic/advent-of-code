<?php

/** @var string[] $input */
$input = file('day8_input.txt');
$total = 0;

foreach ($input as $line) {
    $line = preg_replace("(\r|\n)", '', $line);
    echo "Inspecting line: ";
    var_dump($line);

    $total += countStringLiterals($line);
    $total -= countCharacters($line);
    echo "\n";
}

echo "Found it: {$total}\n";

function countStringLiterals($string)
{
    return mb_strlen($string);
}

function countCharacters($string)
{
    $string = substr($string, 1, -1); // cut initial and trailing qouble quotes
    $length = preg_match_all('#(\\\x[0-9a-f]{2}|\\\{2}|\\\"|\w)#i', $string);

    echo "Length: {$length}\n";
    return $length;
}
