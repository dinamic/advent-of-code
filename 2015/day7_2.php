<?php
$microtime = microtime(true);

/** @var string[] $input */
$input = file('day7_input.txt');

/*$input = array(
    '123 -> x',
    '456 -> y',
    'x AND y -> d',
    'x OR y -> e',
    'x LSHIFT 2 -> f',
    'y RSHIFT 2 -> g',
    'NOT x -> h',
    'NOT y -> i',
);*/

$wires = array();
foreach ($input as $line) {
    $line = preg_replace("(\n|\r)", '', $line);
    $row = explode(' -> ', $line);
    $wires[$row[1]] = $row[0];
}

$wires['b'] = '46065';

function traverseValue($wire, $wires)
{
    static $cache = array();

    if (array_key_exists($wire, $cache)) {
        echo " - cached wire {$wire}!\n";
        return $cache[$wire];
    }

    echo "Checking wire {$wire}\n";
//    usleep(500);

    $matches = array();

    // number
    $regex = '#^(?<source>\d+)$#i';
    if (preg_match($regex, $wires[$wire], $matches)) {
        echo " - matched: {$wires[$wire]} with {$regex}\n";
        return $cache[$wire] = (int) $matches['source'];
    }

    // wire
    $regex = '#^(?<source>\w+)$#i';
    if (preg_match($regex, $wires[$wire], $matches)) {
        echo " - matched: {$wires[$wire]} with {$regex}\n";

        if (is_numeric($matches['source'])) {
            echo " - returning int!\n";
            return $cache[$wire] = (int) $matches['source'];
        }

        return $matches['source'];
    }

    $regex = '#^(?<sourceA>.+) (?<operation>RSHIFT|LSHIFT|OR|AND) (?<sourceB>.+)$#i';
    if (preg_match($regex, $wires[$wire], $matches)) {
        echo " - matched: {$wires[$wire]} with {$regex}\n";

        if (!is_numeric($matches['sourceA'])) {
            echo " + sourceA({$matches['sourceA']}) needs calculating..\n";
            $matches['sourceA'] = traverseValue($matches['sourceA'], $wires);
        }

        if (!is_numeric($matches['sourceB'])) {
            echo " + sourceB({$matches['sourceB']}) needs calculating..\n";
            $matches['sourceB'] = traverseValue($matches['sourceB'], $wires);
        }

        if ('RSHIFT' === $matches['operation']) {
            return $cache[$wire] = $matches['sourceA'] >> $matches['sourceB'];
        }

        if ('LSHIFT' === $matches['operation']) {
            return $cache[$wire] = $matches['sourceA'] << $matches['sourceB'];
        }

        if ('OR' === $matches['operation']) {
            return $cache[$wire] = $matches['sourceA'] | $matches['sourceB'];
        }

        if ('AND' === $matches['operation']) {
            return $cache[$wire] = $matches['sourceA'] & $matches['sourceB'];
        }

        throw new Exception('Unknown bitwise gate: '. $matches['operation']);
    }

    $regex = '#^(?<operation>NOT) (?<sourceA>.+)$#i';
    if (preg_match($regex, $wires[$wire], $matches)) {
        echo " - matched: {$wires[$wire]} with {$regex}\n";

        if (!is_numeric($matches['sourceA'])) {
            echo " + sourceA({$matches['sourceA']}) needs calculating..\n";
            $matches['sourceA'] = traverseValue($matches['sourceA'], $wires);
        }

        return $cache[$wire] = (~ $matches['sourceA']) & 65535;
    }
}

$result = traverseValue('a', $wires);
while (!is_int($result)) {
    echo " Traversing {$result}\n";
    $result = traverseValue($result, $wires);
}

$time = microtime(true) - $microtime;
echo "Run time: ". var_export($time, true) ." seconds \n";
echo "Result: {$result}\n";
