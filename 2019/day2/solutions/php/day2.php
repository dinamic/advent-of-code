<?php

if ($argc !== 2) {
    printf("Usage: php %s <input.txt>\n", $argv[0]);
    exit(1);
}

$input = trim(file_get_contents($argv[1]));

$computer = (new IntCodeComputer())
    ->setDataFromInput($input)
    ->restoreGravityAssist()
    ->calculate();

printf("Part 1: %d\n", $computer->getResult());

foreach (range(0, 99) as $a) {
    foreach (range(0, 99) as $b) {
        $computer
            ->setDataFromInput($input)
            ->restoreGravityAssist($a, $b)
            ->calculate();

        if ($computer->getResult() === 19690720) {
            printf("Part 2: %02d%02d\n", $a, $b);
            break 2;
        }
    }
}


class IntCodeComputer
{
    private const SELF_TEST = [
        '1,0,0,0,99' => '2,0,0,0,99',
        '2,3,0,3,99' => '2,3,0,6,99',
        '2,4,4,5,99,0' => '2,4,4,5,99,9801',
        '1,1,1,4,99,5,6,0,99' => '30,1,1,4,2,5,6,0,99',
    ];

    private const OP_EXIT = 99;
    private const OP_ADD = 1;
    private const OP_MULTIPLY = 2;

    /**
     * @var int[]
     */
    private array $data;

    public function __construct()
    {
        $this->selfTest();
    }

    public function setDataFromInput(string $input): self
    {
        $this->data = array_map(static function (string $data): float {
            return (float) $data;
        }, explode(',', $input));

        return $this;
    }

    private function selfTest(): void
    {
        foreach (self::SELF_TEST as $input => $expectedResult) {
            $this->setDataFromInput($input);
            $this->calculate();

            $actualResult = implode(',', $this->data);

            if ($expectedResult !== $actualResult) {
                throw new LogicException(printf(
                    "Self test failed. Input: %s, Expected: %s, Actual: %s\n",
                    $input,
                    $expectedResult,
                    $actualResult
                ));
            }
        }
    }

    public function restoreGravityAssist(int $a = null, int $b = null): self
    {
        $this->data[1] = $a ?? 12;
        $this->data[2] = $b ?? 2;

        return $this;
    }

    private function processPosition(int $position): bool
    {
        $opCode = $this->data[$position];

        switch ($opCode) {
            default:
                throw new LogicException('Unknown operation');
            case self::OP_EXIT:
                return false;
            case self::OP_ADD:
                $a = $this->data[$this->data[$position+1]];
                $b = $this->data[$this->data[$position+2]];
                $this->data[$this->data[$position+3]] = $a + $b;
                break;
            case self::OP_MULTIPLY:
                $a = $this->data[$this->data[$position+1]];
                $b = $this->data[$this->data[$position+2]];
                $this->data[$this->data[$position+3]] = $a * $b;
                break;
        }

        return true;
    }

    public function calculate(): self
    {
        $opCount = count($this->data);

        for ($i = 0; $i < $opCount; $i += 4) {
            if ($this->processPosition($i)) {
                continue;
            }

            break;
        }

        return $this;
    }

    public function getResult(): int
    {
        return $this->data[0];
    }
}

