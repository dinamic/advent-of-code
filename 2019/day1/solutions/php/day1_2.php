<?php
if ($argc !== 2) {
    printf("Usage: php %s <input.txt>\n", $argv[0]);
    exit(1);
}

$input = file($argv[1]);

$fuelPerModule = array_map(static function (string $moduleMass): float {
    return calculateFuelRecursive((float) $moduleMass);
}, $input);

$totalFuel = array_sum($fuelPerModule);

printf("Part 2: total fuel needed: %f\n", $totalFuel);


function calculateFuelRecursive(float $mass): float
{
    $fuel = floor($mass / 3) - 2;

    if ($fuel <= 0) {
        return 0;
    }

    return $fuel + calculateFuelRecursive($fuel);
}
