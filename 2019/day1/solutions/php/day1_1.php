<?php
if ($argc !== 2) {
    printf("Usage: php %s <input.txt>\n", $argv[0]);
    exit(1);
}

$input = file($argv[1]);

$fuelPerModule = array_map(static function (string $moduleMass): float {
    return floor((float) $moduleMass / 3) - 2;
}, $input);

$totalFuel = array_sum($fuelPerModule);

printf("Part 1: total fuel needed: %f\n", $totalFuel);
