<?php
declare(strict_types=1);

/** @var string[] $input */
$input = file(__DIR__ . '/../input.txt');

$accumulator = 0;
$caloriesPerElf = [];

foreach ($input as $index => $line) {
    $line = preg_replace("(\r|\n)", '', $line);
    echo "Inspecting line: ";
    var_dump($line);

    if ($line === '') {
        $caloriesPerElf[] = $accumulator;
        $accumulator = 0;

        continue;
    }

    if (! ctype_digit($line)) {
        throw new LogicException('Not a number: '. var_export($line, true));
    }

    $accumulator += (int) $line;

    echo "\n";
}

if ($accumulator !== 0) {
    $caloriesPerElf[] = $accumulator;
}

printf("A total of %d elves participated in the input.\n", count($caloriesPerElf));

if (! rsort($caloriesPerElf, SORT_NUMERIC)) {
    throw new LogicException('Failed to sort array');
}

$maxCaloriesValue = reset($caloriesPerElf);
printf("The most calories carried by an elf: %d\n", $maxCaloriesValue);


$topCaloriesValue = array_shift($caloriesPerElf);
$topCaloriesValue += array_shift($caloriesPerElf);
$topCaloriesValue += array_shift($caloriesPerElf);

printf("The calories carried by the top 3 elves: %d\n", $topCaloriesValue);
