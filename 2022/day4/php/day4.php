<?php
declare(strict_types=1);

const INPUT_FILE_PATH = __DIR__ . '/../input.txt';

(new PartOne(INPUT_FILE_PATH))->process();
(new PartTwo(INPUT_FILE_PATH))->process();

class PartOne
{
    private string $inputFilePath;

    public function __construct(string $inputFilePath)
    {
        $this->inputFilePath = $inputFilePath;
    }

    protected function readInputFile(): Generator
    {
        $input = file($this->inputFilePath);

        foreach ($input as $index => $line) {
            $line = preg_replace("(\r|\n)", '', $line);
//            printf("Inspecting line: %s\n", var_export($line, true));

            yield $line;
        }
    }

    public function process(): void
    {
        $total = 0;

        foreach ($this->readInputFile() as $line) {
            $total += $this->processLine($line);
        }

        printf("PART ONE: total is %d\n", $total);
    }

    protected function processLine(string $line): int
    {
        [$key1, $key2] = explode(',', $line);

        $range1 = $this->expandRange($key1);
        $range2 = $this->expandRange($key2);

        $commonIds = array_intersect($range1, $range2);
        $commonIdCount = count($commonIds);

        $isFullyContained = ($commonIdCount === count($range1)) || ($commonIdCount === count($range2));

        return $isFullyContained ? 1 : 0;
    }

    protected function expandRange(string $range): array
    {
        [$startsAt, $endsAt] = explode('-', $range);

        return range($startsAt, $endsAt);
    }
}

class PartTwo extends PartOne
{
    public function process(): void
    {
        $total = 0;

        foreach ($this->readInputFile() as $line) {
            $total += $this->processLine($line);
        }

        printf("PART TWO: total is %d\n", $total);
    }

    protected function processLine(string $line): int
    {
        [$key1, $key2] = explode(',', $line);

        $range1 = $this->expandRange($key1);
        $range2 = $this->expandRange($key2);

        $commonIds = array_intersect($range1, $range2);
        $commonIdCount = count($commonIds);

        return $commonIdCount > 0 ? 1 : 0;
    }
}