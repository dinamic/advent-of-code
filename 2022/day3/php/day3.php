<?php
declare(strict_types=1);

const INPUT_FILE_PATH = __DIR__ . '/../input.txt';

(new PartOne(INPUT_FILE_PATH))->process();
(new PartTwo(INPUT_FILE_PATH))->process();

class PartOne
{
    private string $inputFilePath;

    public function __construct(string $inputFilePath)
    {
        $this->inputFilePath = $inputFilePath;
    }

    public function getTotalScore(): int
    {
        $totalScore = 0;
        $iterator = $this->readInputFile();

        foreach ($iterator as [$opponentChoice, $myChoice]) {
            $turnScore = $this->getTurnScore(
                $this->mapChoiceToShape($opponentChoice),
                $this->mapChoiceToShape($myChoice)
            );

            $totalScore += $turnScore;
        }

        return $totalScore;
    }

    protected function readInputFile(): Generator
    {
        $input = file($this->inputFilePath);

        foreach ($input as $index => $line) {
            $line = preg_replace("(\r|\n)", '', $line);
//            printf("Inspecting line: %s\n", var_export($line, true));

            yield $line;
        }
    }

    public function process(): void
    {
        $total = 0;

        foreach ($this->readInputFile() as $line) {
            $total += array_sum($this->processLine($line));
        }

        printf("PART ONE: total is %d\n", $total);
    }

    public function processLine(string $line): array
    {
        $compartmentItemCount = mb_strlen($line) / 2;
        $contentsA = mb_substr($line, 0, $compartmentItemCount);
        $contentsB = mb_substr($line, $compartmentItemCount);

        $priorityA = $this->convertCompartmentContentsToPriority($contentsA);
        $priorityB = $this->convertCompartmentContentsToPriority($contentsB);

        return array_unique(
            array_intersect($priorityA, $priorityB)
        );
    }

    protected function convertCompartmentContentsToPriority(string $compartmentContents): array
    {
        return array_map(
            fn(string $letter): int => $this->getCharacterPriority($letter),
            str_split($compartmentContents)
        );
    }

    private function getCharacterPriority(string $char): int
    {
        if (mb_strlen($char) !== 1) {
            throw new LogicException('Expecting a single character');
        }

        $priority = ord($char);

        if ($priority >= 97 && $priority <= 122) {
            $priority -= 96;
        } elseif ($priority >= 65 && $priority <= 90) {
            $priority -= 65;
            $priority += 27;
        } else {
            throw new LogicException('Unexpected character: '. var_export($char, true));
        }

//        printf("Character: %s has the code %s\n", $char, $priority);
        return $priority;
    }
}

class PartTwo extends PartOne
{
    public function process(): void
    {
        $chunks = array_chunk(
            iterator_to_array($this->readInputFile()),
            3
        );

        $total = 0;

        foreach ($chunks as $chunk) {
            $total += $this->processChunk($chunk);
        }

        printf("PART TWO: total is %d\n", $total);
    }

    private function processChunk(array $chunk): int
    {
        return array_sum(
            array_unique(
                array_intersect(
                    ...array_map(
                        fn(string $contents): array => $this->convertCompartmentContentsToPriority($contents),
                        $chunk
                    )
                )
            )
        );
    }
}
