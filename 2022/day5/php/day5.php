<?php
declare(strict_types=1);

const INPUT_FILE_PATH = __DIR__ . '/../input.txt';

(new DayFivePartOne(INPUT_FILE_PATH))->process();
(new DayFivePartTwo(INPUT_FILE_PATH))->process();

class DayFivePartOne
{
    private string $inputFilePath;

    public function __construct(string $inputFilePath)
    {
        $this->inputFilePath = $inputFilePath;
    }

    protected function readInputFile(): Generator
    {
        $input = file($this->inputFilePath);

        foreach ($input as $index => $line) {
            $line = preg_replace("(\r|\n)", '', $line);
//            printf("Inspecting line: %s\n", var_export($line, true));

            yield $line;
        }
    }

    public function process(): void
    {
        $state = $this->readInitialState();
        $state = $this->processInstructions($state);

        echo "\nFinal state:\n";
        $this->printState($state);

        $this->printAnswer($state);
    }

    protected function printAnswer(array $state): void
    {
        printf(
            "PART ONE: code is %s\n",
            implode('', array_map(static fn(array $items): string => str_replace(['[', ']'], '', $items[0]), $state))
        );
    }

    protected function readInitialState(): array
    {
        $state = [];

        foreach($this->readInputFile() as $line) {
            if ($line === '') {
                break;
            }

            $data = array_map(
                static fn(array $chars): string => trim(implode('', $chars)),
                array_chunk(
                    str_split($line),
                    4
                )
            );

            if (empty($data)) {
                continue;
            }

            if (ctype_digit(reset($data))) {
                continue;
            }

            foreach ($data as $key => $value) {
                ++$key;

                if (! array_key_exists($key, $state)) {
                    $state[$key] = [];
                }

                if ($value === '') {
                    continue;
                }

                $state[$key][] = $value;
            }
        }

        return $state;
    }
    protected function readInstructions(): Generator
    {
        $isHeader = true;

        foreach($this->readInputFile() as $line) {
            if ($line === '') {
                $isHeader = false;
                continue;
            }

            if ($isHeader) {
                continue;
            }

            yield $line;
        }
    }

    protected function processInstructions(array $state): array
    {
        foreach($this->readInstructions() as $line) {
            $state = $this->processLine($line, $state);
//            echo $line ."\n";
//            $this->printState($state);
        }

        return $state;
    }
    protected function printState(array $state): void
    {
        $longestCount = max(array_map(
            static fn(array $items): int => count($items),
            $state
        ));

        foreach ($state as &$items) {
            while (count($items) < $longestCount) {
                array_unshift($items, '   ');
            }
        }

        $columns = count($state);

        for ($y=0; $y<$longestCount; ++$y) {
            echo '| ';

            for ($x=1; $x<=$columns; ++$x) {
                if ($state[$x] === null) {
                    var_dump($state, $x);
                    exit;
                }

                echo $state[$x][$y] . ' | ';
            }

            echo "\n";
        }

        echo str_repeat('-', 8 * (5+2) -1) ."\n";

        echo '| ';

        foreach (array_keys($state) as $key) {
            echo str_pad((string) ' '. $key, 3, ' ') . ' | ';
        }

        echo "\n\n";
    }

    private function processLine(string $line, array $state): array
    {
        $matches = [];

        if (! preg_match('#(?<action>\w+) (?<quantity>\d+) from (?<origin>\d+) to (?<destination>\d+)#', $line, $matches)) {
            throw new LogicException('Failed to match line. '. var_export($line, true));
        }

        $action = $matches['action'];
        $quantity = (int) $matches['quantity'];
        $origin = (int) $matches['origin'];
        $destination = (int) $matches['destination'];

        if ($action === 'move') {
            return $this->move($quantity, $origin, $destination, $state);
        }

        throw new LogicException('Unsupported action. '. var_export($line, true));
    }

    protected function move(int $iterations, int $origin, int $destination, array $state): array
    {
        if ($iterations < 1) {
            throw new LogicException('Negative or zero iterations are not supported.');
        }

        for ($i=0; $i < $iterations; ++$i) {
            $item = array_shift($state[$origin]);

            if ($item === null) {
                throw new LogicException('NULL values are not supported');
            }

            array_unshift($state[$destination], $item);
        }

        return $state;
    }
}

class DayFivePartTwo extends DayFivePartOne
{
    protected function printAnswer(array $state): void
    {
        printf(
            "PART TWO: code is %s\n",
            implode('', array_map(static fn(array $items): string => str_replace(['[', ']'], '', $items[0]), $state))
        );
    }

    protected function move(int $iterations, int $origin, int $destination, array $state): array
    {
        if ($iterations < 1) {
            throw new LogicException('Negative or zero iterations are not supported.');
        }

        $items = array_slice($state[$origin], 0, $iterations);
        array_unshift($state[$destination], ...$items);

        for ($i=0; $i < $iterations; ++$i) {
            array_shift($state[$origin]);
        }

        return $state;
    }
}
