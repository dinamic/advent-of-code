<?php
declare(strict_types=1);

$instance = new PartOne(__DIR__ . '/../input.txt');
printf("PART ONE: Total score is: %d\n", $instance->getTotalScore());

$instance = new PartTwo(__DIR__ . '/../input.txt');
printf("PART TWO: Total score is: %d\n", $instance->getTotalScore());


class PartOne
{
    protected const ROCK = 1;
    protected const PAPER = 2;
    protected const SCISSOR = 3;

    private const CHOICE_MAP = [
        'A' => self::ROCK,
        'B' => self::PAPER,
        'C' => self::SCISSOR,

        'X' => self::ROCK,
        'Y' => self::PAPER,
        'Z' => self::SCISSOR,
    ];

    private string $inputFilePath;

    public function __construct(string $inputFilePath)
    {
        $this->inputFilePath = $inputFilePath;
    }

    public function getTotalScore(): int
    {
        $totalScore = 0;
        $iterator = $this->readInputFile();

        foreach ($iterator as [$opponentChoice, $myChoice]) {
            $turnScore = $this->getTurnScore(
                $this->mapChoiceToShape($opponentChoice),
                $this->mapChoiceToShape($myChoice)
            );

            $totalScore += $turnScore;
        }

        return $totalScore;
    }

    protected function readInputFile(): Generator
    {
        $input = file($this->inputFilePath);

        foreach ($input as $index => $line) {
            $line = preg_replace("(\r|\n)", '', $line);
            printf("Inspecting line: %s\n", var_export($line, true));

            [$opponentChoice, $myChoice] = explode(' ', $line);

            yield [$opponentChoice, $myChoice];
        }
    }

    private function getTurnScore(int $opponentShape, int $myShape): int
    {
        return $this->getShapeScore($myShape) + $this->getTurnOutcome($opponentShape, $myShape);
    }

    private function getShapeScore(int $shape): int
    {
        $map = [
            self::ROCK => 1,
            self::PAPER => 2,
            self::SCISSOR => 3,
        ];

        return $map[$shape];
    }

    private function getTurnOutcome(int $opponentShape, int $myShape): int
    {
        $isWinningChoice = $this->isWin($opponentShape, $myShape);

        if ($isWinningChoice === null) {
            return 3;
        }

        return $isWinningChoice ? 6 : 0;
    }

    private function isWin(int $opponentShape, int $myShape): ?bool
    {
        if ($opponentShape === $myShape) {
            return null;
        }

        $map = [
            self::ROCK => self::SCISSOR,
            self::PAPER => self::ROCK,
            self::SCISSOR => self::PAPER,
        ];

        $myChoiceCanBeat = $map[$myShape];

        return $opponentShape === $myChoiceCanBeat;
    }

    protected function mapChoiceToShape(string $choice): int
    {
        if (! array_key_exists($choice, self::CHOICE_MAP)) {
            throw new OutOfBoundsException();
        }

        return self::CHOICE_MAP[$choice];
    }
}

class PartTwo extends PartOne
{
    /**
     * Anyway, the second column says how the round needs to end:
     * - X means you need to lose
     * - Y means you need to end the round in a draw
     * - Z means you need to win
     */
    private const ACTION_MAP = [
        'X' => [
            self::ROCK => self::SCISSOR,
            self::PAPER => self::ROCK,
            self::SCISSOR => self::PAPER,
        ],
        'Y' => [
            self::ROCK => self::ROCK,
            self::PAPER => self::PAPER,
            self::SCISSOR => self::SCISSOR,
        ],
        'Z' => [
            self::ROCK => self::PAPER,
            self::PAPER => self::SCISSOR,
            self::SCISSOR => self::ROCK,
        ],
    ];

    protected function readInputFile(): Generator
    {
        $map = [
            self::ROCK => 'A',
            self::PAPER => 'B',
            self::SCISSOR => 'C',
        ];

        foreach (parent::readInputFile() as [$opponentChoice, $action]) {
            $opponentShape = $this->mapChoiceToShape($opponentChoice);

            yield [$opponentChoice, $map[self::ACTION_MAP[$action][$opponentShape]]];
        }
    }
}
