<?php
declare(strict_types=1);

const INPUT_FILE_PATH = __DIR__ . '/../input.txt';

(new DaySixPartOne(INPUT_FILE_PATH))->process();
(new DaySixPartTwo(INPUT_FILE_PATH))->process();

class DaySixPartOne
{
    private string $inputFilePath;

    public function __construct(string $inputFilePath)
    {
        $this->inputFilePath = $inputFilePath;
    }

    protected function readInputFile(): Generator
    {
        $input = file($this->inputFilePath);

        foreach ($input as $index => $line) {
            $line = preg_replace("(\r|\n)", '', $line);
//            printf("Inspecting line: %s\n", var_export($line, true));

            yield $line;
        }
    }

    public function process(): void
    {
        $stream = iterator_to_array($this->readInputFile())[0];

        $offset = $this->findPacket($stream, 4);

//        $offset = $this->findStartOfPacket('mjqjpqmgbljsphdztnvjfqwrcgsmlb');
        printf("PART ONE: offset is %d\n", $offset);
    }

    protected function findPacket(string $stream, int $distinctCharacterAmount): int
    {
        $size = mb_strlen($stream);

        for ($i=0; $i<=$size; ++$i) {
            if ($i+$distinctCharacterAmount > $size) {
                throw new OutOfBoundsException('End of stream reached, yet unable to find start-of-packet');
            }

            $request = mb_substr($stream, $i, $distinctCharacterAmount);
            $uniqueCharacters = count_chars($request, 3);

            if (mb_strlen($uniqueCharacters) !== mb_strlen($request)) {
                continue;
            }

            return $i + $distinctCharacterAmount;
        }

        throw new OutOfBoundsException('Unable to find start-of-packet');
    }
}

class DaySixPartTwo extends DaySixPartOne
{
    public function process(): void
    {
        $stream = iterator_to_array($this->readInputFile())[0];

        $offset = $this->findPacket($stream, 14);

        printf("PART TWO: offset is %d\n", $offset);
    }
}
