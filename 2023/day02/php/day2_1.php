<?php
declare(strict_types=1);

test();

/** @var string[] $input */
$input = file(__DIR__ . '/../input.txt');

$sumOfAllCalibrationValues = 0;

$numbers = array_map(
    static fn (string $line): int => map_line($line, 12, 13, 14),
    $input
);

printf("The sum of all possible game ids is: %d\n", array_sum($numbers));

function map_line(string $line, int $redPoll, int $greenPoll, int $bluePoll): int
{
    $line = trim($line);
    echo "Inspecting line: ";
    var_dump($line);

    [$header, $content] = explode(': ', $line);
    $gameId = (int) str_replace('Game ', '', $header);

    $rounds = array_map(
        static fn (string $round): array => array_reduce(
            explode(', ', $round),
            static function (array $carry, string $data): array {
                [$count, $color] = explode(' ', $data);

                return array_merge(
                    $carry,
                    [
                        $color => (int) $count,
                    ]
                );
            },
            []
        ),
        explode('; ', $content)
    );

    $impossibleRounds = array_filter(
        $rounds,
        static fn (array $round): bool
            => (isset($round['red']) && $round['red'] > $redPoll)
                || (isset($round['green']) && $round['green'] > $greenPoll)
                || (isset($round['blue']) && $round['blue'] > $bluePoll)
    );

    return count($impossibleRounds) > 0
        ? 0
        : $gameId;
}

function test(): void
{
    $input = [
        'Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green' => 1,
        'Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue' => 2,
        'Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red' => 0,
        'Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red' => 0,
        'Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green' => 5,
    ];

    foreach ($input as $line => $result) {
        $actual = map_line($line, 12,13,14);

        if ($actual !== $result) {
            throw new \RuntimeException('Test failed. Expected ' . $result . ', got ' . $actual . '.');
        }
    }
}
