<?php
declare(strict_types=1);

/** @var string[] $input */
$input = file(__DIR__ . '/../input.txt');

$sumOfAllCalibrationValues = 0;

foreach ($input as $index => $line) {
    $line = trim($line);

    echo "Inspecting line: ";
    var_dump($line);

    [$firstNumber, $lastNumber] = find_line_numbers($line);

    $sumOfAllCalibrationValues += (int) ($firstNumber . $lastNumber);

    echo "\n";
}

printf("The sum of all calibration values is: %d\n", $sumOfAllCalibrationValues);

function find_numbers(string $line): array
{
    $firstNumber = preg_match('#(?<number>\d).*#', $line, $matches) ? $matches['number'] : null;
    $lastNumber = preg_match('#.*(?<number>\d)#', $line, $matches) ? $matches['number'] : null;

    return [$firstNumber, $lastNumber];
}
