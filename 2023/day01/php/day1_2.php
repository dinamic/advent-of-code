<?php
declare(strict_types=1);

test();

/** @var string[] $input */
$input = file(__DIR__ . '/../input.txt');

$sumOfAllCalibrationValues = 0;

$numbers = array_map(
    static fn (string $line): int => map_line($line),
    $input
);

printf("The sum of all calibration values is: %d\n", array_sum($numbers));

function map_line(string $line): int
{
    $numbersMap = [
        'one' => 1,
        'nine' => 9,
        'eight' => 8,
        'seven' => 7,
        'six' => 6,
        'five' => 5,
        'four' => 4,
        'three' => 3,
        'two' => 2,
    ];

    $line = trim($line);
    echo "Inspecting line: ";
    var_dump($line);

    $data = str_split($line);
    $numbers = [];

    foreach ($data as $i => $char) {
        if (is_numeric($char)) {
            $numbers[] = $char;
            continue;
        }

        foreach ($numbersMap as $word => $number) {
            if (mb_substr($line, $i, mb_strlen($word)) === $word) {
                $numbers[] = $number;
                break;
            }
        }
    }

    return (int) ($numbers[0] . end($numbers));
}

function find_line_numbers(string $line): array
{
    $firstNumber = preg_match('#(?<number>\d).*#', $line, $matches) ? $matches['number'] : null;
    $lastNumber = preg_match('#.*(?<number>\d)#', $line, $matches) ? $matches['number'] : null;

    return [$firstNumber, $lastNumber];
}

function test(): void
{
    $input = [
        'two1nine' => 29,
        'eightwothree' => 83,
        'abcone2threexyz' => 13,
        'xtwone3four' => 24,
        '4nineeightseven2' => 42,
        'zoneight234' => 14,
        '7pqrstsixteen' => 76,
    ];

    foreach ($input as $line => $result) {
        $actual = map_line($line);

        if ($actual !== $result) {
            throw new \RuntimeException('Test failed. Expected ' . $result . ', got ' . $actual . '.');
        }
    }
}
