<?php
declare(strict_types=1);

test();

/** @var string[] $input */
$input = array_map(
    static fn (string $line): string => trim($line),
    file(__DIR__ . '/../input.txt', FILE_IGNORE_NEW_LINES)
);

$sumOfAllCalibrationValues = 0;

$count = count($input);

$numbers = array_map(
    static fn (string $line, int $lineNo): int => array_sum(map_line($input, $lineNo, $count)),
    $input,
    array_keys($input)
);

printf("The sum of all of the part numbers in the engine schematic: %d\n", array_sum($numbers));

function map_line(array $dataset, int $lineNo, int $totalCount): array
{
    $rangeStart = max(0, $lineNo - 1);
    $rangeEnd = min($lineNo + 2, $totalCount);

    $workingSet = array_slice($dataset, $rangeStart, $rangeEnd - $rangeStart);
    $workingSetLength = count($workingSet);

    if ($workingSetLength === 3) {
        $currentLineNo = 1;
    } elseif ($lineNo - 1 < 0) {
        $currentLineNo = 0;
    } else {
        $currentLineNo = min($workingSetLength - 1, 1);
    }

    $line = $dataset[$lineNo];
    $indentation = str_repeat(' ', 4);

    echo "\nInspecting line: ";
    var_dump($line);

    echo "Working set {$rangeStart} - {$rangeEnd}:\n";
    echo $indentation . implode("\n" . $indentation, $workingSet) . "\n";

    $workingSet = array_map(
        static fn (string $line): array => str_split($line),
        $workingSet
    );

    return find_non_adjacent_to_symbol_numbers($workingSet, $currentLineNo);
}

function find_non_adjacent_to_symbol_numbers(array $workingSet, int $idx): array
{
    $numbers = [];
    $accumulator = '';
    $debug = false;

    foreach ($workingSet[$idx] as $charNo => $char) {
        $debug && print("Inspecting {$char} at {$charNo}\n");

        if (! ctype_digit($char)) {
            if ($accumulator === '') {
                continue;
            }

            if (is_symbol_adjacent($workingSet, $idx, $charNo, $accumulator)) {
                $numbers[] = $accumulator;
            }

            $accumulator = '';
            continue;
        }

        $accumulator .= $char;
    }

    if ($accumulator !== '' && is_symbol_adjacent($workingSet, $idx, $charNo, $accumulator)) {
        $numbers[] = $accumulator;
    }

    echo "Found numbers: " . implode(', ', $numbers) . ". Sum: " . array_sum($numbers) . "\n";

    return array_map('intval', $numbers);
}

function is_symbol_adjacent(array $workingSet, $idx, $charNo, $accumulator): bool
{
    //unset($workingSet[$idx]);
    $length = strlen($accumulator);
    echo 'Checking ' . $accumulator . ' at ' . $charNo . ' with length ' . $length . "\n";

    $data = array_map(
        static fn (array $line): string => implode('', $line),
        $workingSet
    );

    $data = array_filter(
        $data,
        static function (string $line) use ($charNo, $length): bool {
            $rangeStart = max($charNo - $length -1, 0); // starts one character before in order to consider diagonal match
            $rangeEnd = min($length + 2, strlen($line) - $rangeStart); // ends one character after in order to consider diagonal match
            $haystack = substr($line, $rangeStart, $rangeEnd);

            $hasMatches = preg_replace('#(\d|\.)#', '', $haystack) !== '';

            printf(
                'Checking "%s" with range %d:%d - "%s" => %s' .  "\n",
                $line,
                $rangeStart,
                $rangeEnd,
                $haystack,
                $hasMatches ? 'true' : 'false'
            );

            return $hasMatches;
        }
    );

    return count($data) > 0;
}

function test(): void
{
    $input = [
        '467..114..' => 467,
        '...*......' => 0,
        '..35..633.' => 35 + 633,
        '......#...' => 0,
        '617*......' => 617,
        '.....+.58.' => 0,
        '..592.....' => 592,
        '......755.' => 755,
        '...$.*....' => 0,
        '.664.598..' => 664 + 598,
    ];

    $sum = 0;
    $count = count($input);
    $values = array_keys($input);
    $lineNo = 0;

    foreach ($input as $line => $result) {
        $actual = array_sum(map_line($values, $lineNo, $count));

        if ($actual !== $result) {
            throw new \RuntimeException('Test failed. Expected ' . $result . ', got ' . $actual . '.');
        }

        $sum += $actual;
        ++$lineNo;
    }

    assert($sum === 4361);
}
